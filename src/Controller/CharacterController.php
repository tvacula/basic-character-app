<?php

namespace App\Controller;

use App\Entity\Character;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CharacterController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="character_list")
     *
     * @return Response
     */
    public function index(): Response
    {
        $characters = $this->getDoctrine()->getRepository(Character::class)->findAll();

        return $this->render('characters/index.html.twig', [
            'characters' => $characters,
        ]);
    }

    /**
     * @Route("/character/new", methods={"GET", "POST"}, name="new_character")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function new(Request $request): Response
    {
        $character = new Character();
        $form = $this->createFormBuilder($character)
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('bio', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Create',
                'attr' => [
                    'class' => 'btn btn-primary mt-3',
                ],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $character = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($character);
            $entityManager->flush();

            return $this->redirectToRoute('character_list');
        }

        return $this->render('characters/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/character/edit/{id}", methods={"GET", "POST"}, name="edit_character")
     *
     * @param Request $request
     * @param int     $id
     *
     * @return Response
     */
    public function edit(Request $request, int $id): Response
    {
        $character = $this->getDoctrine()->getRepository(Character::class)->find($id);
        $form = $this->createFormBuilder($character)
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('bio', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Update',
                'attr' => [
                    'class' => 'btn btn-primary mt-3',
                ],
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('character_list');
        }

        return $this->render('characters/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/character/{id}", methods={"GET"}, name="character_show")
     *
     * @param $id
     *
     * @return Response
     */
    public function show($id): Response
    {
        $character = $this->getDoctrine()->getRepository(Character::class)->find($id);

        return $this->render('characters/show.html.twig', [
            'character' => $character,
        ]);
    }

    /**
     * @Route("/character/delete/{id}", methods={"DELETE"}, name="delete_character")
     *
     * @param Request $request
     * @param int     $id
     */
    public function delete(Request $request, int $id)
    {
        $character = $this->getDoctrine()->getRepository(Character::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($character);
        $entityManager->flush();

        $response = new Response();
        $response->send();
    }
}
