const characters = document.getElementById("characters");

if (characters) {
    characters.addEventListener("click", (e) => {
        if (e.target.className === "btn btn-danger delete-character") {
            if (confirm("Are you certain?")) {
                const id = e.target.getAttribute("data-id");

                fetch(`/character/delete/${id}`, {
                    method: "DELETE"
                }).then(res => window.location.reload());
            }
        }
    })
}